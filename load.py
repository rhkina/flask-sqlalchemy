import csv
import datetime


from app import models
from app.database import SessionLocal, engine


db = SessionLocal()


models.Base.metadata.create_all(bind=engine)


with open("covid_19_data.csv", "r") as f:
    csv_reader = csv.DictReader(f)

    for row in csv_reader:
        db_record = models.Record(
            date=datetime.datetime.strptime(row["ObservationDate"], "%m/%d/%Y"),
            state=row["Province/State"],
            country=row["Country/Region"],
            cases=row["Confirmed"],
            deaths=row["Deaths"],
            recoveries=row["Recovered"],
        )
        db.add(db_record)

    db.commit()

db.close()
