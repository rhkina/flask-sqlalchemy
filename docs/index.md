# Use Flask and SQLalchemy, not Flask-SQLAlchemy
> Avoiding Flask-SQLAlchemy in Flask apps

Based on [Edward Krueger](https://towardsdatascience.com/use-flask-and-sqlalchemy-not-flask-sqlalchemy-5a64fafe22a4) post

See repository at [GitLab](https://gitlab.com/rhkina/flask-sqlalchemy)

## What is SQLAlchemy?

SQLAlchemy is a Python SQL toolkit and Object Relational Mapper (ORM) that 
allows app developers to use SQL for smooth and fault-tolerant transactional 
database operations. The ORM translates Python classes to tables for 
relational databases and automatically converts Pythonic SQLAlchemy Expression 
Language to SQL statements. This conversion allows developers to write SQL 
queries with Python syntax. SQLAlchemy also abstracts database connections 
and provides connection maintenance automatically. Together these features 
make SQLAlchemy a fantastic package for loading and querying databases.

## What is Flask?

Flask is a microframework that allows you to build web apps in Python. Flask 
is easy to get started with as a beginner because there is little boilerplate 
code for getting a simple app up and running.
For example, here is a valid “Hello, world!” Flask web app:

```python
from flask import Flask
app = Flask(__name__)
@app.route('/')
def hello_world():
    return "Hello, World!"
if __name__ == '__main__':
    app.run()
```

## What is Flask-SQLAlchemy?

Flask-SQLAlchemy is an extension for Flask that aims to simplify using 
SQLAlchemy with Flask by providing defaults and helpers to accomplish common 
tasks. One of the most sought after helpers being the handling of a database 
connection across the app. However, ensuring your database connection session 
is available throughout your app can be accomplished with base SQLAlchemy and 
does not require Flask_SQLAlchemy.

Flask-SQLAlchemy’s purpose is to handle the return of connections to prevent 
issues with worker threading. These issues arise when an app user switches 
from one route to another. Below is a common error that occurs when a 
threading problem is present in a Flask app.

```bash
sqlite3.ProgrammingError: SQLite objects created in a thread can only be used 
in that same thread. The object was created in thread id 12345 and this 
is thread id 54321.
```

This is often caused by a session or database connection being unavailable to 
part of your app. This error effectively breaks your app and must be resolved 
to continue development.

## Flask-SQLAlchemy weaknesses

We opt to avoid the Flask-SQLALchemy default behaviors that prevent this 
error and instead use SQLAlchemy features to handle these issues. This is 
because Flask-SQLALchemy has disadvantages when compared with SQLAlchemy.

One of which is that Flask-SQLAlchemy has its own API. This adds complexity 
by having its different methods for ORM queries and models separate from the 
SQLAlchemy API.

Another disadvantage is that Flask-SQLAlchemy makes using the database 
outside of a Flask context difficult. This is because, with Flask-SQLAlchemy, 
the database connection, models, and app are all located within the `app.py` 
file. Having models within the app file, we have limited ability to interact 
with the database outside of the app. This makes loading data outside of your 
app difficult. Additionally, this makes it hard to retrieve data outside of 
the Flask context.

Flask and SQLAlchemy work well together if used correctly. Therefore, you 
don’t have to hybridize Flask and SQLAlchemy into Flask-SQLalchemy!

## Use SQLAlchemy, not Flask-SQLAlchemy
### Define database and models once

Ideally, you should only have to define your database models once! With a 
separate `database.py` and `models.py` file, we can establish our database 
connection and classes for its tables a single time, then call them later as 
needed. This separation of components is much more difficult with 
Flask-SQLAlchemy as you would have to create the database with the app itself.

Here is a file that defines our database connection using SQLAlchemy.

```python
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

SQLALCHEMY_DATABASE_URL = "sqlite:///./test.db"

engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()
```

Notice that we import the `Base` class in the `database.py` file above into 
the `models.py` file below to use `declarative_base()`.

```python
from sqlalchemy import Column, Integer, String
from sqlalchemy.types import Date
from .database import Base


class Record(Base):
    __tablename__ = "Records"

    id = Column(Integer, primary_key=True, index=True)
    date = Column(Date)
    state = Column(String(255))
    country = Column(String(255), index=True)
    cases = Column(Integer)
    deaths = Column(Integer)
    recoveries = Column(Integer)
```

This file creates the model or schema for the table `Records` in our database.

Using SQLAlcehmy’s `declarative_base()` allows you to write just one model per 
table that app uses. That model is then used in Python outside of the app 
and in the database.

###Loading data outside of the app

Having these separate Python files is good because you can use the same model 
to query or load data outside of an app. Additionally, you’ll have one version 
of each model and database connection, which simplifies development.

These models and database connections can be used to reference the same 
models or databases in data pipelines, report generation, and anywhere else 
they are needed.

The load script alone is a great reason to use SQLAlchemy. Instead of using 
the app to load data, this functionality allows the loading of a database 
with a separate Python file.

Here is an example Python file that reads data from a CSV and inserts that 
data into a database.

```python
import csv
import datetime


from app import models
from app.database import SessionLocal, engine


db = SessionLocal()


models.Base.metadata.create_all(bind=engine)


with open("covid_19_data.csv", "r") as f:
    csv_reader = csv.DictReader(f)

    for row in csv_reader:
        db_record = models.Record(
            date=datetime.datetime.strptime(row["ObservationDate"], "%m/%d/%Y"),
            state=row["Province/State"],
            country=row["Country/Region"],
            cases=row["Confirmed"],
            deaths=row["Deaths"],
            recoveries=row["Recovered"],
        )
        db.add(db_record)

    db.commit()

db.close()
```

###Declarative Base and MetaData

The `declarative_base()` base class contains a `MetaData` object where newly 
defined `Table` objects are collected. This `MetaData` object is accessed 
when we call the line `models.Base.metadata.create_all()` to create all of 
our tables.

###Scoped Session and Session Local: Handling Threading Issues

SQLAlchemy includes a helper object that helps with the establishment of 
user-defined `Session` scopes. With the `scoped_session` function, SQLAlchemy 
can handle worker threading issues.

The `sessionmaker` is a factory for initializing new `Session` objects by 
requesting a connection from the engine’s connection pool and attaching a 
connection to the new `Session` object.

Initializing a new session object is also referred to as “checking out” a 
connection. The database stores a list of these connections/processes. So 
when you begin a new session, you are starting a new process within the 
database too.

A `scoped_session` is the registry of all these created session objects 
where the key/identity of the registry is some form of a thread-safe id.

We define `SessionLocal` in the `database.py` file above by calling the 
session factory, `sessionmaker`, and passing it some parameters.

```python
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
```

The`scopefunc` is an optional argument passed to scoped_session that serves 
as an identity getter function that returns a key to lookup or to register a 
new session. Adding `_app_ctx_stack.__ident_func__` is one of two functions:

1. If greenlet is installed, it uses the `getcurrent` (`from greenlet import 
getcurrent`)
2. Otherwise, it uses `get_ident` (`from threading import get_ident`), which 
returns the thread id.

By default `scopefunc` is `get_ident` so for simple applications, you can 
just do:

```python
db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))
```

This new `SessionLocal` allows different sections of the application to call 
upon a global `scoped_session`, so that all app routes can share the same 
session without passing the session to the route explicitly. 
The `SessionLocal` we've established in our registry will remain, until we 
explicitly tell our registry to dispose of it, by calling 
`scoped_session.remove()`.

Here we can see the call to scoped_session with our custom session, 
`SessionLocal`, passed as an argument.

```python
from flask import Flask, _app_ctx_stack, jsonify, url_for
from flask_cors import CORS
from sqlalchemy.orm import scoped_session

from . import models
from .database import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)

app = Flask(__name__)

CORS(app)

app.session = scoped_session(SessionLocal, scopefunc=_app_ctx_stack.__ident_func__)
```

Additionally, scoped sessions give us access to a `query_property`. So if you 
style used by `flask_sqlalchemy` you can use this with SQLAlchemy:

```python
Base = declarative_base()
Base.query = db_session.query_property()
```

###Closing transactions
The method `db_session.close()` only ends the transaction for the local 
session object, but does not end the connection with the database and does 
not automatically return the connection to the pool.

By adding a `db_session.remove()` we ensure our connection is closed properly.
The `db_session.remove()` method first runs `app.db_session.close()` and 
afterward returns the connection to the connection pool. Therefore we have 
terminated that process locally and at the remote database. If the database 
doesn't have these connections closed, there is a maximum number of 
connections that can be reached. The database will eventually kill idle 
processes like stale connections; however, it can take hours before that 
happens. SQLAlchemy has some pool options to prevent this, but removing the 
connections when they are no longer needed is best!
