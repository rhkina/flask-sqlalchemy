##Based on [Edward Krueger](https://towardsdatascience.com/use-flask-and-sqlalchemy-not-flask-sqlalchemy-5a64fafe22a4) post

Create and activate virtual environment:

```bash
virtualenv venv
source venv/bin/activate
```


Run locally with:

```bash
pip install -r requirements.txt
export FLASK_APP=app.app
flask run --reload
```

In another window:

```bash
sqlite_web test.db
```

See documentation at [https://rhkina.gitlab.io/flask-sqlalchemy/](https://rhkina.gitlab.io/flask-sqlalchemy/)

To see the documentation locally install [`mkdocs`](https://www.mkdocs.org/#installation), run mkdocs server:

```bash
mkdocs serve
```

And access the docs in your browser at: `http://127.0.0.1:8000/`